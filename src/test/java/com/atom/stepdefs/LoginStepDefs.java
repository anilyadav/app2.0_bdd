package com.atom.stepdefs;

import java.io.IOException;

import com.atom.appiumaction.AppiumEngine;
import com.atom.pageobjects.LoginPageObjects;

import cucumber.api.java.Before;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import io.appium.java_client.AppiumDriver;
import io.appium.java_client.MobileElement;
import junit.framework.Assert;

public class LoginStepDefs {
  
	
	public AppiumDriver<MobileElement> driver;
	
	@Before
	public void getDriver() throws IOException{
		AppiumEngine engine = new AppiumEngine();
		driver = engine.getDriver();
	}

	 
	@Given("^user is on login page$")
	public void spalshScreen(){
		System.out.println("Waiting for login screen");
	}
	
	@When("^user enters valid username and password and clicks on submit$")
	public void loginWithValidCredetials(){
	    LoginPageObjects login  = new LoginPageObjects(driver);
		login.login("company", "company");
	}
	
	@Then("^user should succssefully logged in$")
	public void verifySuccessfullLogin(){
		 LoginPageObjects login  = new LoginPageObjects(driver);
		Assert.assertTrue("Unable to login with the given credetials", login.verifySuccessfulLogin());
	}

}
