package com.atom.pageobjects;

import org.openqa.selenium.support.PageFactory;

import com.atom.appiumaction.AppiumActions;

import io.appium.java_client.AppiumDriver;
import io.appium.java_client.MobileElement;
import io.appium.java_client.pagefactory.AndroidFindBy;
import io.appium.java_client.pagefactory.AppiumFieldDecorator;

public class LoginPageObjects extends AppiumActions {
	AppiumDriver<MobileElement> driver;
	
	public LoginPageObjects(AppiumDriver<MobileElement> driver) {
		super(driver);
		this.driver = driver;
		PageFactory.initElements(new AppiumFieldDecorator(driver), this);
	}

	@AndroidFindBy(xpath = "//*[@id='usernameTextField']")
	public MobileElement username;
	
	@AndroidFindBy(xpath = "//*[@id='passwordTextField']")
	public MobileElement password;
	
	@AndroidFindBy(xpath = "//*[@id='loginButton']")
	public MobileElement submit;
	
	@AndroidFindBy(xpath = "//*[@id='makePaymentButton']")
	public MobileElement payment;
	
	
	/**
	 * This method is used to login into the application 
	 * @param uname
	 * @param pwd
	 */
	public void login(String uname, String pwd){
		username.sendKeys("complany");
		password.sendKeys("compnay");
		submit.click();
	}
	
	/**
	 * This method is to validate weather user successfully logged in or not
	 * @return
	 */
	public boolean verifySuccessfulLogin(){
		return payment.isDisplayed();
	}
}
