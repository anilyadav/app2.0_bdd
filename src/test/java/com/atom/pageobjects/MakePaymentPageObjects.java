package com.atom.pageobjects;

import org.openqa.selenium.support.PageFactory;

import com.atom.appiumaction.AppiumActions;

import io.appium.java_client.AppiumDriver;
import io.appium.java_client.MobileElement;
import io.appium.java_client.pagefactory.AndroidFindBy;

public class MakePaymentPageObjects extends AppiumActions{
	AppiumDriver<MobileElement> driver;
	
	public MakePaymentPageObjects(AppiumDriver<MobileElement> driver){
		super(driver);
		this.driver = driver;
		PageFactory.initElements(driver, this);
	}
	
	@AndroidFindBy(xpath = "//*[@id='makePaymentButton']")
	public MobileElement makePayment;
}
