package SampleAppium.Appium;

import io.appium.java_client.remote.AndroidMobileCapabilityType;
import io.appium.java_client.AppiumDriver;
import io.appium.java_client.MobileElement;
import io.appium.java_client.android.AndroidDriver;
import io.appium.java_client.remote.MobileCapabilityType;

import org.openqa.selenium.By;
import org.openqa.selenium.ScreenOrientation;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.testng.annotations.*;



import java.net.URL;
import java.net.MalformedURLException;
import java.util.logging.Level;

public class FirstTest {
	//private String accessKey = "eyJ4cC51IjoyMzQzMzQ1LCJ4cC5wIjoyMzQzMzQ0LCJ4cC5tIjoiTVRVME1EazNNVEEwTnpreE1RIiwiYWxnIjoiSFMyNTYifQ.eyJleHAiOjE4NTYzMzEwNDgsImlzcyI6ImNvbS5leHBlcml0ZXN0In0.7Mgaqva4vHbJaWIZM9n-kn0tIgtyYE5FprD076aaqXU";
	protected AppiumDriver<MobileElement> driver = null;
    DesiredCapabilities dc = new DesiredCapabilities();
    private String reportDirectory = "reports";
    private String reportFormat = "xml";
    private String testName = "Untitled";
  

   
    @BeforeMethod
    public void setUp() throws MalformedURLException {
        dc.setCapability("reportDirectory", reportDirectory);
        dc.setCapability("reportFormat", reportFormat);
        dc.setCapability("testName", testName);
       // dc.setCapability("deviceName", "SM-G935F");
        dc.setCapability(MobileCapabilityType.UDID, "9885754d464d393450");
        dc.setCapability(AndroidMobileCapabilityType.APP_PACKAGE, "com.experitest.ExperiBank");
        dc.setCapability(AndroidMobileCapabilityType.APP_ACTIVITY, ".LoginActivity");
        driver = new AndroidDriver<MobileElement>(new URL("http://localhost:4723/wd/hub"), dc);
        driver.setLogLevel(Level.INFO);
    }

    @Test
    public void quickStartAndroidNativeDemo() throws InterruptedException {
        //driver.rotate(ScreenOrientation.PORTRAIT);
    	Thread.sleep(5000);
        driver.findElement(By.xpath("//*[@id='usernameTextField']")).sendKeys("company");
        driver.hideKeyboard();
        driver.findElement(By.xpath("//*[@id='passwordTextField']")).sendKeys("company");
        driver.findElement(By.xpath("//*[@id='loginButton']")).click();
        driver.findElement(By.xpath("//*[@id='makePaymentButton']")).click();
        driver.findElement(By.xpath("//*[@id='phoneTextField']")).sendKeys("0541234567");
        driver.findElement(By.xpath("//*[@id='nameTextField']")).sendKeys("Jon Snow");
        driver.findElement(By.xpath("//*[@id='amountTextField']")).sendKeys("50");
        driver.findElement(By.xpath("//*[@id='countryButton']")).click();
        driver.findElement(By.xpath("//*[@text='Switzerland']")).click();
        driver.findElement(By.xpath("//*[@id='sendPaymentButton']")).click();
        driver.findElement(By.xpath("//*[@text='Yes']")).click();
    }

    @AfterTest
    public void tearDown() {
        driver.quit();
    }


}