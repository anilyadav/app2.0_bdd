package com.atom.appiumaction;

import java.io.FileInputStream;

import java.io.IOException;
import java.io.InputStream;


import java.net.URL;

import org.openqa.selenium.remote.DesiredCapabilities;


import cucumber.api.java.After;
import cucumber.api.java.Before;
import io.appium.java_client.AppiumDriver;
import io.appium.java_client.MobileElement;
import io.appium.java_client.android.AndroidDriver;
import io.appium.java_client.ios.IOSDriver;
import io.appium.java_client.remote.AndroidMobileCapabilityType;
import io.appium.java_client.remote.IOSMobileCapabilityType;
import io.appium.java_client.remote.MobileCapabilityType;

import java.util.Properties;
import java.util.logging.Level;

public class AppiumEngine {
	
	protected AppiumDriver<MobileElement> driver = null;
    DesiredCapabilities dc = new DesiredCapabilities();
    private String reportDirectory = "reports";
    private String reportFormat = "xml";
    private String testName = "Untitled";
    public static Properties prop = new Properties();
    static InputStream input = null;

    @Before
	public AppiumDriver<MobileElement> getDriver() throws IOException{
		dc.setCapability("reportDirectory", reportDirectory);
        dc.setCapability("reportFormat", reportFormat);
        dc.setCapability("testName", testName);
        input = new FileInputStream("config.properties");
        prop.load(input);
        if(prop.getProperty("platform").equals("Android")){
        dc.setCapability(MobileCapabilityType.UDID, "9885754d464d393450");
        dc.setCapability(AndroidMobileCapabilityType.APP_PACKAGE, "com.experitest.ExperiBank");
        dc.setCapability(AndroidMobileCapabilityType.APP_ACTIVITY, ".LoginActivity");
		driver = new AndroidDriver<MobileElement>(new URL("http://localhost:4723/wd/hub"), dc);
        }
        else
        {
            dc.setCapability(MobileCapabilityType.UDID, "0c2cb7d0c42a764cf9ea11e594ce24eb82357b2e");
            dc.setCapability(IOSMobileCapabilityType.BUNDLE_ID, "com.experitest.ExperiBank");
            driver = new IOSDriver<>(new URL("http://localhost:4723/wd/hub"), dc);
            driver.setLogLevel(Level.INFO); 
        }
		return driver;
		
	}
    
    @After
    public void tearDown() {
        driver.quit();
    }
	
}
