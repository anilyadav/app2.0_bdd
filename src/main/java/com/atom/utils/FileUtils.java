package com.atom.utils;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Properties;

import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

public class FileUtils {

	public static final String TODAY = "today";
	public static final String YESTERDAY = "yesterday";
	public static final String LAST_WEEK = "last week";
	public static final String LAST_MONTH = "last month";
	public static final String LAST_YEAR = "last year";
	public static final String WEEK_TO_DATE = "week to date";
	public static final String MONTH_TO_DATE = "month to date";
	public static final String COMMA_DELIMITER = ",";
	public static final String NEW_LINE_SEPARATOR = "\n";
	static FileWriter fileWriter = null;

	static Properties props = 
			openPropertyFile(new File(System.getProperty("user.dir") + File.separator + "UAT2Config.properties"));

	public void writeCsvFile(String fileNames, String TestSteps, String TestDecription, String Status,
			String ImageName) {

		String fileName = System.getProperty("user.dir") + File.separator + props.getProperty("resultsTempPath")
				+ File.separator + fileNames + ".csv";
		try {
			fileWriter = new FileWriter(fileName, true);

			// TestSteps,TestDecription,Time,Status,ImageName
			ImageName = ImageName.replace(".png", "");
			String data = TestSteps + COMMA_DELIMITER + TestDecription + COMMA_DELIMITER + getCurrentTime()
					+ COMMA_DELIMITER + Status + COMMA_DELIMITER + System.getProperty("user.url") + COMMA_DELIMITER
					+ ImageName + NEW_LINE_SEPARATOR;
			fileWriter.append(data);

		} catch (Exception e) {
			System.out.println("Error in CsvFileWriter !!!");
			e.printStackTrace();
		} finally {

			try {
				fileWriter.flush();
				fileWriter.close();
			} catch (IOException e) {
				System.out.println("Error while flushing/closing fileWriter !!!");
				e.printStackTrace();
			}

		}
	}

	public static String readCsvFile(String fileName, String Key) {
		String line = "";
		String cvsSplitBy = ",";
		String[] data = null;
		String keyValue = null;
		try (BufferedReader br = new BufferedReader(new FileReader(fileName))) {

			while ((line = br.readLine()) != null) {
				data = line.split(cvsSplitBy);
				if (data[0].equalsIgnoreCase(Key)) {
					keyValue = String.valueOf(data[1]);
					break;
				}

			}

		} catch (IOException e) {
			e.printStackTrace();
		}
		return keyValue;

	}

	public static String readCsvFile(String fileName, String Key, int colnumber) {
		String line = "";
		String cvsSplitBy = ",";
		String[] data = null;
		String keyValue = null;
		try (BufferedReader br = new BufferedReader(new FileReader(fileName))) {

			while ((line = br.readLine()) != null) {
				data = line.split(cvsSplitBy);
				if (data[0].equalsIgnoreCase(Key)) {
					keyValue = data[colnumber];
					break;
				}

			}

		} catch (IOException e) {
			e.printStackTrace();
		}
		return keyValue;

	}

	public void writeintoJSON(String fileName, String key, String value) throws IOException, ParseException {

		JSONParser parser = new JSONParser();
		Object obj = parser.parse(new FileReader(fileName));

		JSONObject jsonObject = (JSONObject) obj;

		FileWriter fileWriter = new FileWriter(fileName);
		jsonObject.put(key, value);
		fileWriter.write(jsonObject.toJSONString());
		fileWriter.flush();
		fileWriter.close();
	}

	public String readJSON(String fileName, String key) {
		JSONParser parser = new JSONParser();
		Object obj = null;
		try {
			obj = parser.parse(new FileReader(fileName));
		} catch (IOException e) {
			e.printStackTrace();
		} catch (ParseException e) {
			e.printStackTrace();
		}
		JSONObject jsonObject = (JSONObject) obj;

		String keyValue = (String) jsonObject.get(key);

		return keyValue;
	}

	

	public static Properties openPropertyFile(File file) {
		Properties propertyFile = new Properties();
		try {
			FileInputStream fis = new FileInputStream(file);
			propertyFile.load(fis);
			fis.close();
		} catch (IOException e) {
			e.printStackTrace();
			propertyFile = null;
		}
		return propertyFile;
	}

	public static ArrayList<String> readFileLineByLine(File file) {
		ArrayList<String> lines = new ArrayList<String>();
		try {
			FileInputStream fis = new FileInputStream(file);
			BufferedReader br = new BufferedReader(new InputStreamReader(fis, "UTF-8"));
			String strLine;
			while ((strLine = br.readLine()) != null) {
				lines.add(strLine);
			}
			br.close();
		} catch (IOException ioe) {

		}
		return lines;
	}

	public static String getDateFromStr(String range) {
		String dateRange = "dates[";
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy_MM_dd");
		Calendar calendar = Calendar.getInstance();
		if (range.equals(TODAY)) {
			dateRange += sdf.format(calendar.getTime()) + "," + sdf.format(calendar.getTime()) + "]";
		} else if (range.equals(YESTERDAY)) {
			calendar.add(Calendar.DATE, -1);
			dateRange += sdf.format(calendar.getTime()) + "," + sdf.format(calendar.getTime()) + "]";
		} else if (range.equals(LAST_WEEK)) {
			calendar.set(Calendar.DAY_OF_WEEK, Calendar.MONDAY);
			calendar.add(Calendar.DATE, -7);
			dateRange += sdf.format(calendar.getTime()) + ",";
			calendar.add(Calendar.DATE, 6);
			dateRange += sdf.format(calendar.getTime()) + "]";
		} else if (range.equals(LAST_MONTH)) {
			calendar.set(Calendar.DAY_OF_MONTH, 1);
			calendar.add(Calendar.MONTH, -1);
			dateRange += sdf.format(calendar.getTime()) + ",";
			calendar.add(Calendar.DATE, calendar.getActualMaximum(Calendar.DATE) - 1);
			dateRange += sdf.format(calendar.getTime()) + "]";
		} else if (range.equals(LAST_YEAR)) {
			calendar.set(Calendar.DAY_OF_YEAR, 1);
			calendar.add(Calendar.YEAR, -1);
			dateRange += sdf.format(calendar.getTime()) + ",";
			calendar.add(Calendar.YEAR, 1);
			calendar.add(Calendar.DATE, -1);
			dateRange += sdf.format(calendar.getTime()) + "]";
		} else if (range.equals(WEEK_TO_DATE)) {
			String today = sdf.format(calendar.getTime());
			calendar.set(Calendar.DAY_OF_WEEK, Calendar.MONDAY);
			dateRange += sdf.format(calendar.getTime()) + "," + today + "]";
		} else if (range.equals(MONTH_TO_DATE)) {
			String today = sdf.format(calendar.getTime());
			calendar.set(Calendar.DAY_OF_MONTH, 1);
			dateRange += sdf.format(calendar.getTime()) + "," + today + "]";
		}
		return dateRange;
	}

	public static boolean removeFolder(File removeFile) {

		if (removeFile.exists()) {
			File[] files = removeFile.listFiles();
			if (files != null) {
				for (File f : files) {
					f.delete();
				}
			}
			return removeFile.delete();
		}
		return false;
	}

	public static String getDateStamp() {
		String stamp = new SimpleDateFormat("yyMMdd").format(Calendar.getInstance().getTime());
		return stamp;
	}

	public static String getTimeStamp() {
		String stamp = new SimpleDateFormat("yyMMddkkmm").format(Calendar.getInstance().getTime());
		return stamp;
	}

	public static String getTimeSecondsStamp() {
		String stamp = new SimpleDateFormat("yyMMddkkmmss").format(Calendar.getInstance().getTime());
		return stamp;
	}

	public static String getTodayDate() {
		String startDate = new SimpleDateFormat("dd/MM/yyyy").format(Calendar.getInstance().getTime());
		return startDate;
	}

	public static String getTodayDateNewFormat() {
		String startDate = new SimpleDateFormat("dd-MM-yyyy").format(Calendar.getInstance().getTime());
		String date[] = startDate.split("-");
		String month = "";
		switch (date[1]) {
		case "01":
			month = "January";
			break;
		case "02":
			month = "February";
			break;
		case "03":
			month = "March";
			break;
		case "04":
			month = "April";
			break;
		case "05":
			month = "May";
			break;
		case "06":
			month = "June";
			break;
		case "07":
			month = "July";
			break;
		case "08":
			month = "August";
			break;
		case "09":
			month = "September";
			break;
		case "10":
			month = "October";
			break;
		case "11":
			month = "November";
			break;
		case "12":
			month = "December";
			break;
		default:
			month = null;
		}

		String newdate = date[0] + "-" + month + "-" + date[2];

		return newdate;
	}

	public static String getCurrentTime() {
		String currentTime = new SimpleDateFormat("hh:mm:ss a").format(Calendar.getInstance().getTime());
		return currentTime;
	}

	public static String getNextDate() {
		SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
		Calendar cal = Calendar.getInstance();
		cal.add(Calendar.DATE, 1);
		if (cal.get(Calendar.DAY_OF_WEEK) == 1) {
			cal.add(Calendar.DATE, 1);
		}
		if (cal.get(Calendar.DAY_OF_WEEK) == 7) {
			cal.add(Calendar.DATE, 2);
		}
		String nextDate = sdf.format(cal.getTime());
		return nextDate;
	}

	public static String getPreviousDate() {
		SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
		Calendar cal = Calendar.getInstance();
		cal.add(Calendar.DATE, -1);
		if (cal.get(Calendar.DAY_OF_WEEK) == 1) {
			cal.add(Calendar.DATE, -2);
		}
		if (cal.get(Calendar.DAY_OF_WEEK) == 7) {
			cal.add(Calendar.DATE, -1);
		}
		String nextDate = sdf.format(cal.getTime());
		return nextDate;
	}

}
